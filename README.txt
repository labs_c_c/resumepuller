resume_puller.py was written from the source resumeScraper.py

Author: Cyle DeLucca
Company: iSenpai, LLC.

Designed to prompt user for a search term, which it wraps in the indeed search url for us to use later.

Next we ask the user for their password which we utilize pythons getpass module to secure.

Once that is done we proceed to login to indeed.com and search based on the search term we defined earlier.

We then iterate over the links to determine how many times we need to increment our index.

The index correlates with page numbers (ie. (start=0)=page1, (start=50)=page2, ..)

While iterating over the multiple pages of results, we are gathering each resume's specific site-index number.

Once we have reached the end of all the results we start to dive down into each page.

Using the resume's site-index we append the url and visit the page directly.

Next we grab the html of the page and store it for later use.

For each page we visit we grab the html and open a file which is named after the resume-contact information from the html.

The results are an html file named as follows: <resume-contact +  "_" + query + "_.html">
					example: john-doe_query.html